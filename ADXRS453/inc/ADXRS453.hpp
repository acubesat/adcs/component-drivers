#pragma once

#include "FreeRTOS.h"
#include "Logger.hpp"
#include "Peripheral_Definitions.hpp"
#include "etl/expected.h"
#include "task.h"
#include <cstdint>

/**
 * Choose between eqm software and software for atsam development board. 0 is the fault and is for eqm, 1 is for development board.
 */
#define SW_USED 0

/**
 * Select which SPI of ATSAMV71Q21B MCU will be used. By giving the value 0 or 1 to ADXRS453_SPI_PORT the driver will
 * use SPI0 or SPI1 respectively.
 */
#define ADXRS453_SPI_PORT 0

#if SW_USED==0
 #if ADXRS453_SPI_PORT == 0
#include "peripheral/spi/spi_master/plib_spi0_master.h"
#define ADXRS_WriteRead SPI0_WriteRead
#define ADXRS_IsTransmitterBusy SPI0_IsTransmitterBusy
#define ADXRS_Initialize SPI0_Initialize

 #else
#include "peripheral/spi/spi_master/plib_spi1_master.h"
#define ADXRS_WriteRead SPI1_WriteRead
#define ADXRS_IsBusy SPI1_IsTransmitterBusy
#define ADXRS_Initialize SPI1_Initialize
 #endif
#endif

#if SW_USED==1
#include "peripheral/spi/spi_master/plib_spi0_master.h"
#define ADXRS_WriteRead SPI0_WriteRead
#define ADXRS_IsTransmitterBusy SPI0_IsTransmitterBusy
#define ADXRS_Initialize SPI0_Initialize
#endif

/**
 * This is a generic driver implementation for ADXRS453, based on the ADXRS453
datasheet.
 * @ingroup drivers
 * @see
https://www.analog.com/media/en/technical-documentation/data-sheets/adxrs453.pdf
 * @author Xhulio Luli <lulixhulio@gmail.com>
 */
class ADXRS453 {
public:
    enum class ADXRS453Error : uint8_t {
        NO_ERROR,
        INVALID_ARG,
        MEASUREMENT_FAILED,
        DEVICE_FAIL,
        AMPLITUDE_DETECTION_FAIL,
        OVERVOLTAGE,
        UNDERVOLTAGE,
        PHASE_LOCKED_LOOP_FAIL,
        QUADRATURE_FAIL,
        NONVOLATILE_MEMORY_FAULT,
        POWER_ON_OR_RESET_FAIL,
        POWER_REGULATION_FAIL,
        CONTINUOUS_SELF_TEST_OR_AMP_DETECTION_FAIL
    };

     /**
     * Max number of errors that can be identified based on page 18 table 14 of the datasheet.
     */
     constexpr static uint8_t MaxInternalErrors = 10;

    /**
     * @param ssn is the chip select pin of the SPI peripheral.
     */
    ADXRS453(PIO_PIN ssn);

    /*
     * Transforms a unsigned integer to signed by checking its MSB. If MSB is 1
     * then its twos complement representation and adds 1
     * @param data the data to be checked
     */
    int16_t getSignedData(uint16_t &data) {
        uint16_t negative = data & 0b10000000'00000000;
        int16_t signedData =
            (data & 0b1000000000) ? -(~(data | 0b11111100'00000000) + 1) : data;
        return signedData;
    }

    /**
     * Get the rate in deg/s.
     */
    etl::expected<float, ADXRS453Error> getRate();

    /**
     * Get the gyroscope's temperature in degree celsius.
     */
    etl::expected<float, ADXRS453Error> getTemperatureValue();

    /**
     * Get the value of the temperature compensated and low-pass filtered
     * continuous self-test delta. This value is a measure of the difference
     * between the positive and negative self-test deflections and corresponds
     * to the values presented in Table 1 (datasheet pg. 3).
     */
    etl::expected<uint16_t, ADXRS453Error> getLowContinuousSelfTestFailure();

    /**
     * Get the unfiltered self-test information. This data can be used to
     * supplement fault diagnosis in safety critical applications because sudden
     * shifts in the self-test response can be detected.
     */
    etl::expected<uint16_t, ADXRS453Error> getHighContinuousSelfTestFailure();

    /*
     * Get the value corresponding to the amount of quadrature error present in
     * the device at a given time measured in deg/sec.
     */
    etl::expected<uint16_t, ADXRS453Error> getQuadratureError();

    /*
     * Get the state of the error flags in the device.
     */
    etl::expected<uint16_t, ADXRS453Error> getErrorFlags();

    /**
     * Get the gyroscope's revision ID.
     */
    etl::expected<uint16_t, ADXRS453Error> getRevisionID();

    /*
     * Get the identification number that uniquely identifies the device.
     */
    etl::expected<uint32_t, ADXRS453Error> getSerialNumber();

     /*
     * Get the internal errors by checking the error flags
     */
    etl::array<ADXRS453Error, MaxInternalErrors> getInternalError();


private:
    /**
     * Chip select pin of the SPI peripheral.
     */
    PIO_PIN ssn;

    /**
     * Wait period before a sensor read is skipped
     */
    static constexpr uint16_t TimeoutTicks = 1000;

    /**
     *Value returned when no actual communication is taking place, corresponds to a 8 ones (only High read at pins)
     */
    static constexpr uint8_t FalseData = 255;

    /**
     * Address of each register.
     */
    enum class RegisterAddress : uint8_t {
        RATE1 = 0x00,
        RATE0 = 0x01,
        TEM1 = 0x02,
        TEM0 = 0x03,
        LOCST1 = 0x04,
        LOCST0 = 0x05,
        HICST1 = 0x06,
        HICST0 = 0x07,
        QUAD1 = 0x08,
        QUAD0 = 0x09,
        FAULT1 = 0x0A,
        FAULT0 = 0x0B,
        PID1 = 0x0C,
        PID0 = 0x0D,
        SN3 = 0x0E,
        SN2 = 0x0F,
        SN1 = 0x10,
        SN0 = 0x11
    };

    /**
     * First 3 bits of each command prepares the device to read/write according
     * to the datasheet.
     */
    enum class Commands {
        Read = 0b10000000,
        Write = 0b01000000,
    };

    /**
     * Measurement modes of the gyroscope.
     */
    enum class Modes : uint8_t {
        ReadRate = 0,
        ReadTemperature = 1,
        ReadLOCST = 2,
        ReadHICST = 3,
        ReadQUAD = 4,
        ReadErrorFlags = 5,
        ReadRevisionID = 6,
        ReadSerialNumberFirstPart = 7,
        ReadSerialNumberSecondPart = 8,
        None = 255,
    };

    /**
     * Shows from which register the previous data was read. Every time a read
     * command is sent to the device, it will answer to the previous
     * read-command. If getPID, getRate or getTemperature function is being
     * called, it will return a value from a previous read-command. lastReadMode
     * is used inside getPID, getRate and getTemperature functions to check the
     * last-read-mode. If the last-read-mode is different from the function's
     * mode, a read command will be sent to clear the SPI-port register from the
     * previous read-mode value and to prepare the current read-mode value.
     *
     *         ╔═══════════════════╗          ╔═══════════════════╗
     *  MOSI-> ║ Command N         ║          ║ Command N+1       ║
     *         ╚═══════════════════╝          ╚═══════════════════╝
     *         ╔══════════════════════════╗   ╔══════════════════════════╗
     *  MISO-> ║ Response to N-1 Command  ║   ║ Response to N Command    ║
     *         ╚══════════════════════════╝   ╚══════════════════════════╝
     *
     * @see DEVICE DATA LATCHING section of the datasheet.
     */
    Modes lastReadMode = Modes::None;

    /**
     * Step of linear interpolation which is used to calculate the temperature
     * value, based on table-17 data from ADXRS453 datasheet.
     */
    constexpr static float Step = 0.2f;
    /**
     * Intercept of linear interpolation which is used to calculate the
     * temperature value, based to table-17 data from ADXRS453 datasheet.
     */
    constexpr static float Intercept = 45.0f;

    /**
     * Scale factor of the rate.
     */
    constexpr static float ScaleFactor = 80.0f;

    /**
     * Calculates the temperature value via linear interpolation as defined by
     * the ADXRS453 datasheet pg. 23
     */
    inline float linearInterpolation(int16_t data) {
        static_assert(Step >= 0);
        static_assert(Intercept > 0.0f);

        return data * Step + Intercept;
    }

    /**
     * Returns true if command even odd number of ones.
     * @param command the read/write command.
     */
    bool hasEvenOnes(uint32_t command) {
        return __builtin_popcount(command) % 2 == 0;
    }

    /**
     * Function that prevents hanging when SPI Chip Select is stuck.
     */
    inline void waitForResponse() {
        auto start = xTaskGetTickCount();
        while (ADXRS_IsTransmitterBusy()) {
            if (xTaskGetTickCount() - start > TimeoutTicks) {
                LOG_ERROR << "Gyroscope at pin" << ssn << " has timed out";
                ADXRS_Initialize();
            }
            taskYIELD();
        }
    };

    /**
     * Send a command to Read from a specific register of the ADXRS453 device
     * and gets the value from the SPI-port register of a previous command.
     * @param registerAddress is the value of a register address.
     */
    etl::expected<uint16_t, ADXRS453::ADXRS453Error>
    sendCommandToReadFromRegisterAndGetPreviousValue(uint8_t registerAddress);

    /**
     * Writes a byte to a specific address.
     * @param registerAddress is the specific address in which a byte is going
     * to be written.
     * @param data is the byte which will be written to the specific register.
     */
    ADXRS453Error writeToRegister(uint8_t registerAddress, uint16_t data);
};
