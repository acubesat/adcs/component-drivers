## ADXRS453 Gyroscope Driver

This is a generic driver for ATSAMV71 SPI0 and SPI1 peripherals based on "ADSRX453 datasheet".

To test the driver, the SPI0 peripheral from ATSAMV71Q21B dev board was used, as shown below:

| SPI0                      | ADXRS453 | 
|---------------------------|:--------:|
| MOSI (PD21 - EXT1 PIN 16) |    SI    | 
| MISO (PD20 - EXT1 PIN 17) |    SO    |
| NPCL (PD22 - EXT1 PIN 18) |   CLK    |
| NPCS0 (PB2 - EXT1 PIN 6)  |   SSN    |


where NPCS0 (PB2) was used as a GPIO pin.

## Basic ADXRS453 Class functions
- checkPID(): Get the gyroscope's revision ID.
- ADXRS453(): Sets the chip select pin.
- getTemperatureValue(): Get the gyroscope's temperature value.
- getRate(): Get the rate.


