#include "ADXRS453.hpp"

ADXRS453::ADXRS453(PIO_PIN ssn) : ssn(ssn) {
    PIO_PinWrite(ssn, true);
    getRevisionID();
}

etl::expected<uint16_t, ADXRS453::ADXRS453Error> ADXRS453::sendCommandToReadFromRegisterAndGetPreviousValue(uint8_t registerAddress) {
    etl::array<uint8_t, 4> txData = {};
    etl::array<uint8_t, 4> rxData = {};

    txData[0] = static_cast<std::underlying_type_t<Commands>>(Commands::Read);
    txData[1] = registerAddress << 1;
    txData[2] = 0b00000000;
    txData[3] = hasEvenOnes(txData[0] << 8 | txData[1]);

    PIO_PinWrite(ssn, false);

    if(not ADXRS_WriteRead(txData.data(), 4, rxData.data(), 4)) {
        return etl::unexpected(ADXRS453Error::INVALID_ARG);
    }
    waitForResponse();

    PIO_PinWrite(ssn, true);

    if (rxData[0] == FalseData && rxData[1] == FalseData && rxData[2] == FalseData && rxData[3] == FalseData) {
        return etl::unexpected(ADXRS453Error::MEASUREMENT_FAILED);
    }

    return (static_cast<uint16_t>(rxData[1]) << (3 + 8)) |
           static_cast<uint16_t>(rxData[2] << 3) |
           static_cast<uint16_t>(rxData[3] >> 5);
}

[[nodiscard]] ADXRS453::ADXRS453Error ADXRS453::writeToRegister(uint8_t registerAddress,
                                                        uint16_t data) {
    etl::array<uint8_t, 4> txData = {};

    txData[0] = static_cast<std::underlying_type_t<Commands>>(Commands::Write);
    txData[1] = (registerAddress << 1) | (data >> 15);
    txData[2] = (data | 0b011111111'0000000) >> 8;
    txData[3] = ((data | 0b00000000'11111111) << 1) |
                hasEvenOnes(txData[0] << 24 | txData[1] << 16 | txData[2] << 8 |
                            txData[3]);

    PIO_PinWrite(ssn, false);

    if (not ADXRS_WriteRead(txData.data(), 4, nullptr, 0)) {
        return ADXRS453Error::INVALID_ARG;
    }
    waitForResponse();

    PIO_PinWrite(ssn, true);

    return ADXRS453Error::NO_ERROR;
}

etl::expected<float, ADXRS453::ADXRS453Error> ADXRS453::getRate() {
    if (lastReadMode != Modes::ReadRate) {
        sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<uint8_t>(RegisterAddress::RATE1));
        lastReadMode = Modes::ReadRate;
    }
    auto data = sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<uint8_t>(RegisterAddress::RATE1));
    if(not data.has_value()) {
        return etl::unexpected(data.error());
    }

    uint16_t Negative = data.value() & 0b10000000'00000000;
    int16_t SignedData = ADXRS453::getSignedData(data.value());

    return SignedData / ScaleFactor;
}

etl::expected<float, ADXRS453::ADXRS453Error> ADXRS453::getTemperatureValue() {
    if (lastReadMode != Modes::ReadTemperature) {
        sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::TEM1));
        lastReadMode = Modes::ReadTemperature;
    }
    auto data = sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::TEM1));
    if(not data.has_value()) {
        return etl::unexpected(data.error());
    }

    data.value() >>= 6;
    int16_t signedData = ADXRS453::getSignedData(data.value());
    (data.value() & 0b1000000000) ? -(~(data.value() | 0b11111100'00000000) + 1) : data.value();
    return linearInterpolation(signedData);
}

etl::expected<uint16_t, ADXRS453::ADXRS453Error>
ADXRS453::getLowContinuousSelfTestFailure() {
    if (lastReadMode != Modes::ReadLOCST) {
        sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::LOCST1));
        lastReadMode = Modes::ReadLOCST;
    }

    auto data = sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::LOCST1));

    if (not data.has_value()) {
        return etl::unexpected(data.error());
    }

    return data.value();
}

etl::expected<uint16_t, ADXRS453::ADXRS453Error>
ADXRS453::getHighContinuousSelfTestFailure() {
    if (lastReadMode != Modes::ReadHICST) {
        sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::HICST1));
        lastReadMode = Modes::ReadHICST;
    }

    auto data = sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::HICST1));

    if (not data.has_value()) {
        return etl::unexpected(data.error());
    }

    return data.value();
}

etl::expected<uint16_t, ADXRS453::ADXRS453Error> ADXRS453::getQuadratureError() {
    if (lastReadMode != Modes::ReadQUAD) {
        sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::QUAD1));
        lastReadMode = Modes::ReadQUAD;
    }

    auto data = sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::QUAD1));

    if (not data.has_value()) {
        return etl::unexpected(data.error());
    }

    return data.value();
}

etl::expected<uint16_t, ADXRS453::ADXRS453Error> ADXRS453::getErrorFlags() {
    if (lastReadMode != Modes::ReadErrorFlags) {
        sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::FAULT1));
        lastReadMode = Modes::ReadErrorFlags;
    }

    auto data = sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::FAULT1));

    if (not data.has_value()) {
        return etl::unexpected(data.error());
    }

    return data.value();
}

etl::expected<uint16_t, ADXRS453::ADXRS453Error> ADXRS453::getRevisionID() {
    if (lastReadMode != Modes::ReadRevisionID) {
        sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::PID1));
        lastReadMode = Modes::ReadRevisionID;
    }

    auto data = sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::PID1));

    if (not data.has_value()) {
        return etl::unexpected(data.error());
    }

    return data.value();
}

etl::expected<uint32_t, ADXRS453::ADXRS453Error> ADXRS453::getSerialNumber() {
    if (lastReadMode != Modes::ReadSerialNumberFirstPart) {
        sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::SN3));
        lastReadMode = Modes::ReadSerialNumberFirstPart;
    }
    auto data1 = sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::SN3));
    if (not data1.has_value()) {
        return etl::unexpected(data1.error());
    }

    if (lastReadMode != Modes::ReadSerialNumberSecondPart) {
        sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::SN1));
        lastReadMode = Modes::ReadSerialNumberSecondPart;
    }
    auto data2 = sendCommandToReadFromRegisterAndGetPreviousValue(static_cast<std::underlying_type_t<RegisterAddress>>(RegisterAddress::SN1));

    if (not data2.has_value()) {
        return etl::unexpected(data2.error());
    }

    return (data1.value() << 16) | data2.value();
}

etl::array<ADXRS453::ADXRS453Error, ADXRS453::MaxInternalErrors> ADXRS453::getInternalError() {
    etl::array<ADXRS453Error, ADXRS453::MaxInternalErrors> InternalErrors = {ADXRS453Error::NO_ERROR};
    uint8_t index = 0;

    auto result = getErrorFlags();
    if(not result.has_value()) {
        InternalErrors[index] = result.error();
        return InternalErrors;
    }

    if((result.value() & 0b0000100000000000) != 0) {
        InternalErrors[index++] = ADXRS453Error::DEVICE_FAIL;
    }
    if((result.value() & 0b0000001000000000) != 0) {
        InternalErrors[index++] = ADXRS453Error::OVERVOLTAGE;
    }
    if((result.value() & 0b0000000100000000) != 0) {
        InternalErrors[index++] = ADXRS453Error::UNDERVOLTAGE;
    }
    if((result.value() & 0b0000000010000000) != 0) {
        InternalErrors[index++] = ADXRS453Error::PHASE_LOCKED_LOOP_FAIL;
    }
    if((result.value() & 0b0000000001000000) != 0) {
        InternalErrors[index++] = ADXRS453Error::QUADRATURE_FAIL;
    }
    if((result.value() & 0b0000000000100000) != 0) {
        InternalErrors[index++] = ADXRS453Error::NONVOLATILE_MEMORY_FAULT;
    }
    if((result.value() & 0b0000000000010000) != 0) {
        InternalErrors[index++] = ADXRS453Error::POWER_ON_OR_RESET_FAIL;
    }
    if((result.value() & 0b0000000000000100) != 0) {
        InternalErrors[index++] = ADXRS453Error::CONTINUOUS_SELF_TEST_OR_AMP_DETECTION_FAIL;
    }

    return InternalErrors;
}
