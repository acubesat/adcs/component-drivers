#include "RM3100.hpp"

etl::expected<uint8_t, RM3100::RM3100Error>  RM3100::getREVID() {
    return readFromRegister(RegisterAddress::REVID);
}

etl::expected<uint8_t, RM3100::RM3100Error> RM3100::readFromRegister(RegisterAddress registerAddress) {
    uint8_t rxData = 0;
    PIO_PinWrite(ssn, false);
    uint8_t txData = static_cast<uint8_t>(registerAddress) | 0b10000000;

    if (not RM3100_WriteRead(&txData, 1, nullptr, 0)){
        return etl::unexpected(RM3100Error::INVALID_ARG);
    }
    waitForResponse();

    if (not RM3100_WriteRead(nullptr, 0, &rxData, 1)){
        return etl::unexpected(RM3100Error::INVALID_ARG);
    }
    waitForResponse();

    PIO_PinWrite(ssn, true);

    if(rxData == FalseData) {
        return etl::unexpected(RM3100Error::MEASUREMENT_FAILED);
    }

    return rxData;
}

[[nodiscard]] RM3100::RM3100Error RM3100::writeToRegister(RegisterAddress registerAddress, uint8_t txData) {
    PIO_PinWrite(ssn, false);

    if (not RM3100_WriteRead(&registerAddress, 1, nullptr, 0)){
    	return RM3100Error::INVALID_ARG;
    }
    waitForResponse();

    if (not RM3100_WriteRead(&txData, 1, nullptr, 0)){
    	return RM3100Error::INVALID_ARG;
    }
    waitForResponse();

    PIO_PinWrite(ssn, true);

    return RM3100Error::NO_ERROR;
}

[[nodiscard]] RM3100::RM3100Error RM3100::setCycleCount(uint16_t cycleCountX, uint16_t cycleCountY, uint16_t cycleCountZ) {
    uint8_t msb = cycleCountX >> 8;
    uint8_t lsb = cycleCountX & 0b0000000011111111;
    auto result1 = writeToRegister(RegisterAddress::cycleCountX_MSB, msb);
    auto result2 = writeToRegister(RegisterAddress::cycleCountX_LSB, lsb);
    if(result1 != RM3100Error::NO_ERROR) {
    	return result1;
    }
    if(result2 != RM3100Error::NO_ERROR) {
    	return result2;
    }

    gainX = calculateGainFromCycleCount(cycleCountX);

    msb = cycleCountY >> 8;
    lsb = cycleCountY & 0b0000000011111111;
    result1 = writeToRegister(RegisterAddress::cycleCountY_MSB, msb);
    result2 = writeToRegister(RegisterAddress::cycleCountY_LSB, lsb);
    if(result1 != RM3100Error::NO_ERROR) {
    	return result1;
    }
    if(result2 != RM3100Error::NO_ERROR) {
    	return result2;
    }

    gainY = calculateGainFromCycleCount(cycleCountY);

    msb = cycleCountZ >> 8;
    lsb = cycleCountZ & 0b0000000011111111;
    result1 = writeToRegister(RegisterAddress::cycleCountZ_MSB, msb);
    result2 = writeToRegister(RegisterAddress::cycleCountZ_LSB, lsb);
    if(result1 != RM3100Error::NO_ERROR) {
    	return result1;
    }
    if(result2 != RM3100Error::NO_ERROR) {
    	return result2;
    }
    gainZ = calculateGainFromCycleCount(cycleCountZ);
	return RM3100Error::NO_ERROR;
}

etl::expected<etl::array<uint16_t, 3>, RM3100::RM3100Error> RM3100::getCycleCount() {
    etl::array<uint16_t, 3> cycleCountOfEachAxis;

    auto msb = readFromRegister(RegisterAddress::cycleCountX_MSB);
    auto lsb = readFromRegister(RegisterAddress::cycleCountX_LSB);
    if(not msb.has_value()) {
    	return etl::unexpected(msb.error());
    }
    if(not lsb.has_value()) {
    	return etl::unexpected(lsb.error());
    }
    cycleCountOfEachAxis[0] = msb.value() << 8 | lsb.value();

    msb = readFromRegister(RegisterAddress::cycleCountY_MSB);
    lsb = readFromRegister(RegisterAddress::cycleCountY_LSB);
    if(not msb.has_value()) {
    	return etl::unexpected(msb.error());
    }
    if(not lsb.has_value()) {
    	return etl::unexpected(lsb.error());
    }
    cycleCountOfEachAxis[1] = msb.value() << 8 | lsb.value();

    msb = readFromRegister(RegisterAddress::cycleCountZ_MSB).value();
    lsb = readFromRegister(RegisterAddress::cycleCountZ_LSB).value();
    if(not msb.has_value()) {
    	return etl::unexpected(msb.error());
    }
    if(not lsb.has_value()) {
    	return etl::unexpected(lsb.error());
    }
    cycleCountOfEachAxis[2] = msb.value() << 8 | lsb.value();

    return cycleCountOfEachAxis;
}

[[nodiscard]] RM3100::RM3100Error RM3100::setContinuousMeasurementMode() {
    uint8_t cmmStatus = 0b0;
    uint8_t triggerDRDY = 0b10;
    uint8_t pollValue = 0b01110000;

    cmmStatus |= continuousMeasurementMode | (triggerDRDY << 2) | pollValue;
    return writeToRegister(RegisterAddress::CMM, cmmStatus);
}

[[nodiscard]] RM3100::RM3100Error RM3100::setSingleMeasurementMode() {
    uint8_t pollValue = 0b01110000;
    return writeToRegister(RegisterAddress::POLL, pollValue);
}

RM3100::RM3100(bool continuousMeasurementMode, uint16_t cycleCountX, uint16_t cycleCountY, uint16_t cycleCountZ,
               PIO_PIN ssn, PIO_PIN drdy)
        : continuousMeasurementMode(continuousMeasurementMode), ssn(ssn), drdy(drdy) {
    PIO_PinWrite(ssn, true);
    setCycleCount(cycleCountX, cycleCountY, cycleCountZ);
    setContinuousMeasurementMode();
}

float RM3100::calculateGainFromCycleCount(uint16_t cycleCount) {
    static_assert(std::is_unsigned<decltype(cycleCount)>());
    static_assert(step >= 0);
    static_assert(intercept > 0);

    return step * cycleCount + intercept;
}

[[nodiscard]] RM3100::RM3100Error RM3100::readMeasurements() {
    auto data = readFromRegister(RegisterAddress::STATUS);
    if(not data.has_value()) {
        return data.error();
    }
    if (data.value() & 0b10000000) {
        int32_t x = 0, y = 0, z = 0;

        auto xMeasurement2 = readFromRegister(RegisterAddress::X2);
        if(not xMeasurement2.has_value()){return xMeasurement2.error();}
        auto xMeasurement1 = readFromRegister(RegisterAddress::X1);
        if(not xMeasurement1.has_value()){return xMeasurement1.error();}
        auto xMeasurement0 = readFromRegister(RegisterAddress::X0);
        if(not xMeasurement0.has_value()){return xMeasurement0.error();}
        auto yMeasurement2 = readFromRegister(RegisterAddress::Y2);
        if(not yMeasurement2.has_value()){return yMeasurement2.error();}
        auto yMeasurement1 = readFromRegister(RegisterAddress::Y1);
        if(not yMeasurement1.has_value()){return yMeasurement1.error();}
        auto yMeasurement0 = readFromRegister(RegisterAddress::Y0);
        if(not yMeasurement0.has_value()){return yMeasurement0.error();}
        auto zMeasurement2 = readFromRegister(RegisterAddress::Z2);
        if(not zMeasurement2.has_value()){return zMeasurement2.error();}
        auto zMeasurement1 = readFromRegister(RegisterAddress::Z1);
        if(not zMeasurement1.has_value()){return zMeasurement1.error();}
        auto zMeasurement0 = readFromRegister(RegisterAddress::Z0);
        if(not zMeasurement0.has_value()){return zMeasurement0.error();}

        x = (xMeasurement2.value() & 0b10000000) ? 0b11111111 : 0;
        y = (yMeasurement2.value() & 0b10000000) ? 0b11111111 : 0;
        z = (zMeasurement2.value() & 0b10000000) ? 0b11111111 : 0;
        x = (x << 24) | (static_cast<uint32_t>(xMeasurement2.value()) << 16) | (static_cast<uint16_t>(xMeasurement1.value()) << 8) |
            xMeasurement0.value();
        y = (y << 24) | (static_cast<uint32_t>(yMeasurement2.value()) << 16) | (static_cast<uint16_t>(yMeasurement1.value()) << 8) |
            yMeasurement0.value();
        z = (z << 24) | (static_cast<uint32_t>(zMeasurement2.value()) << 16) | (static_cast<uint16_t>(zMeasurement1.value()) << 8) |
            zMeasurement0.value();

        magneticFieldX = x / gainX;
        magneticFieldY = y / gainY;
        magneticFieldZ = z / gainZ;

        magneticFieldNorm = sqrt(pow(x / gainX, 2) + pow(y / gainY, 2) + pow(z / gainZ, 2));
        return RM3100Error::NO_ERROR;
    }

}

etl::expected<float, RM3100::RM3100Error> RM3100::getDataRate() {
    constexpr float MaxRate = 600.0f;
    constexpr uint8_t MinTMRCValue = 0x92;
    auto rxData = readFromRegister(RegisterAddress::TMRC);
    if(not rxData.has_value()){
    	return etl::unexpected(rxData.error());
    }
    return MaxRate / pow(2, rxData.value() - MinTMRCValue);
}

[[nodiscard]] RM3100::RM3100Error RM3100::setDataRate(float frequency) {
    assert(frequency > 0 && frequency <= 600);
    uint8_t MinTMRCValue = 0x92;
    uint8_t txData = round(log2(600 / frequency) + MinTMRCValue);
    return writeToRegister(RegisterAddress::TMRC, txData);
}

etl::expected<uint8_t, RM3100::RM3100Error> RM3100::selfTest() {
    setSingleMeasurementMode();

    uint8_t txData = 0b10001111;
    writeToRegister(RegisterAddress::BIST, txData);
    vTaskDelay(pdMS_TO_TICKS(100));

    auto rxData = readFromRegister(RegisterAddress::BIST);

    writeToRegister(RegisterAddress::BIST, 0);

    setContinuousMeasurementMode();

    if(not rxData.has_value()) {
        return etl::unexpected(RM3100Error::MEASUREMENT_FAILED);
    }

    return rxData.value();
}
