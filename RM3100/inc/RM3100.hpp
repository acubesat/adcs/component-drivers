#pragma once

#include <cstdint>
#include <cmath>
#include <etl/array.h>
#include "FreeRTOS.h"
#include "task.h"
#include "Peripheral_Definitions.hpp"
#include "Logger.hpp"
#include "etl/expected.h"
#include "etl/optional.h"

/**
 * Choose between eqm software and software for atsam development board. 0 is the fault and is for eqm, 1 is for development board.
 */
#define SW_USED 0

/**
 * Select which SPI of ATSAMV71Q21B MCU will be used. By giving the value 0 or 1 to RM3100_SPI_PORT the driver will
 * use SPI0 or SPI1 respectively.
 */
#define RM3100_SPI_PORT 1

#if SW_USED==0
 #if RM3100_SPI_PORT == 0
 #include "peripheral/spi/spi_master/plib_spi0_master.h"
 #define RM3100_WriteRead SPI0_WriteRead
 #define RM3100_IsTransmitterBusy SPI0_IsTransmitterBusy
 #define RM3100_Initialize SPI0_Initialize


 #else
 #include "peripheral/spi/spi_master/plib_spi1_master.h"
 #define RM3100_WriteRead SPI1_WriteRead
 #define RM3100_IsTransmitterBusy SPI1_IsTransmitterBusy
 #define RM3100_Initialize SPI1_Initialize
#endif
#endif

#if SW_USED==1
#include "peripheral/spi/spi_master/plib_spi0_master.h"
#define RM3100_WriteRead SPI0_WriteRead
#define RM3100_IsTransmitterBusy SPI0_IsTransmitterBusy
#define RM3100_Initialize SPI0_Initialize
#endif

/**
 * This is a generic driver implementation for RM3100, based on "RM3100 Testing Boards User Manual".
 * @ingroup drivers
 * @see https://www.pnicorp.com/wp-content/uploads/RM3100-Testing-Boards-User-Manual-r04-1.pdf
 * @author Xhulio Luli <lulixhulio@gmail.com>
 */
class RM3100 {
public:
    enum class RM3100Error : uint8_t {
        NO_ERROR,
        INVALID_ARG,
        MEASUREMENT_FAILED,
        Z_OSCILLATOR_FAIL,
        Y_OSCILLATOR_FAIL,
        X_OSCILLATOR_FAIL,
        INVALID_SELF_TEST
    };

    /**
     * Max number of errors that can be identified with the Build-in self test
     */
    static constexpr uint8_t MaxNumberOfSelfTestErrors = 3;

    /**
     * @param continuousMeasurementMode is used to set the mode, 1 for Continuous Measurement Mode and 0 for Single Measurement Mode.
     * @param cycleCount is the number of sensor oscillation cycles that will be counted during a measurement sequence, where 200 is the default value.
     * @param ssn is the chip select pin of the SPI peripheral.
     * @param drdy is the data read status pin of magnetometer.
     */
    RM3100(bool continuousMeasurementMode, uint16_t cycleCountX, uint16_t cycleCountY, uint16_t cycleCountZ,
           PIO_PIN ssn, PIO_PIN drdy);

    /*
     * Function that checks if magnetometer is alive.
     */
    bool isAlive() {
        if(getREVID() == revisionID) {
            return true;
        }
        return false;
    }

    double getMagneticFieldNorm() {
        return magneticFieldNorm;
    }

    double getX() {
        return magneticFieldX;
    }

    double getY() {
        return magneticFieldY;
    }

    double getZ() {
        return magneticFieldZ;
    }

    float getGainX() {
        return gainX;
    }

    float getGainY() {
        return gainY;
    }

    float getGainZ() {
        return gainZ;
    }

    RM3100Error setCycleCount(uint16_t cycleCountX, uint16_t cycleCountY, uint16_t cycleCountZ);

    /**
     * Gets the cycle count of each axis.
     */
    etl::expected<etl::array<uint16_t, 3>, RM3100Error> getCycleCount();

    /**
     * Get the magnetometer's revision ID.
     */
    etl::expected<uint8_t, RM3100Error> getREVID();

    /**
     * Reads the magnetic field and updates the internal variables.
     */
    RM3100Error readMeasurements();

    /**
     * @return the Continuous Measurement Mode Data Rate in Hz.
     */
    etl::expected<float, RM3100Error> getDataRate();

    /**
     * Sets the Continuous Measurement Mode Data Rate.
     * @param frequency is the new data rate in Hz.
     * @note before passing any arg as frequency, table 5-4 of RM3100 datasheet should be checked.
     */
    RM3100Error setDataRate(float frequency);

    /**
     * Runs a self test to check if X, Y, and Z functions correctly.
     */
    etl::expected<uint8_t, RM3100Error> selfTest();

private:
    enum class RegisterAddress : uint8_t {
        POLL = 0x00,
        CMM = 0x01,
        cycleCountX_MSB = 0x04,
        cycleCountX_LSB = 0x05,
        cycleCountY_MSB = 0x06,
        cycleCountY_LSB = 0x07,
        cycleCountZ_MSB = 0x08,
        cycleCountZ_LSB = 0x09,
        TMRC = 0x0B,
        X2 = 0x24,
        X1 = 0x25,
        X0 = 0x26,
        Y2 = 0x27,
        Y1 = 0x28,
        Y0 = 0x29,
        Z2 = 0x2A,
        Z1 = 0x2B,
        Z0 = 0x2C,
        BIST = 0x33,
        STATUS = 0x34,
        REVID = 0x36,
    };
    /**
     * Unique revision ID
     */
    const uint8_t revisionID = 0;

    /**
     * Wait period before a sensor read is skipped
     */
    const uint16_t TimeoutTicks = 1000;

    /**
     * Step of linear interpolation which is used to calculate the gain, based on table 3-1 data from RM3100 datasheet.
     */
    constexpr static float step = 0.367143f;

    /**
     * Intercept of linear interpolation which is used to calculate the gain, based on table 3-1 data from RM3100 datasheet.
     */
    constexpr static float intercept = 1.5f;

    /**
     * Set to 1 for Continuous Measurement Mode and 0 for Single Measurement Mode for the RM3100 magnetometer.
     * */
    bool continuousMeasurementMode;

    /**
     * Chip select pin of the SPI peripheral.
     */
    PIO_PIN ssn;

    /**
     * Data ready pin of the magnetometer.
     */
    PIO_PIN drdy;

    /**
     * Gain by which every measurement is multiplied.
     */
    float gainX = 75.0f;

    /**
     * Gain by which every measurement is multiplied.
     */
    float gainY = 75.0f;

    /**
     * Gain by which every measurement is multiplied.
     */
    float gainZ = 75.0f;

    /**
     * Magnetic field of each axis in uT.
     */
    double magneticFieldX = 0, magneticFieldY = 0, magneticFieldZ = 0;

    /**
     * Magnetic field magnitude in uT.
     */
    double magneticFieldNorm = 0;

    /**
     *Value returned when no actual communication is taking place, corresponds to a 8 ones (only High read at pins)
     */
    static constexpr uint8_t FalseData = 255;

    /**
    * Function that prevents hanging when SPI Chip Select is stuck.
    */
    inline void waitForResponse() {
        auto start = xTaskGetTickCount();
        while (RM3100_IsTransmitterBusy()) {
            if (xTaskGetTickCount() - start > TimeoutTicks) {
                LOG_ERROR << "Gyroscope at pin" << ssn
                          << " has timed out";
                RM3100_Initialize();

            }
            taskYIELD();
        }
    };

    /**
     * Sets the magnetometer to take measurements automatically on a regular frequency.
     */
    RM3100Error setContinuousMeasurementMode();

    /**
     * Sets the magnetometer to read values by polling for single measurement.
     */
    RM3100Error setSingleMeasurementMode();

    /**
     * Calculates the cycle-count dependent gain via linear interpolation.
     */
    float calculateGainFromCycleCount(uint16_t cycleCount);

    /**
     * Reads from a specific register of the RM3100 device.
     * @param registerAddress is the value of a register address.
     */
    etl::expected<uint8_t, RM3100Error> readFromRegister(RegisterAddress registerAddress);

    /**
     * Writes a byte to a specific address.
     * @param registerAddress is the specific address in which a byte is going to be written.
     * @param txData is the byte which will be written to the specific register.
     */
    RM3100Error writeToRegister(RegisterAddress registerAddress, uint8_t txData);

};
