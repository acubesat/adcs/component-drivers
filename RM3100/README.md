## RM3100 Magnetometer Driver

This is a generic driver for the ATSAMV71 SPI0 and SPI1 peripherals based on ["RM3100 Testing Boards User Manual"](https://www.pnicorp.com/wp-content/uploads/RM3100-Testing-Boards-User-Manual-r04-1.pdf).

To test the driver, the SPI0 peripheral from ATSAMV71Q21B dev board was used, as shown below:

| SPI0        | RM3100 | 
|-------------|:-------------:|
| MOSI (PD21) | SI |
| MISO (PD20) | SO | 
| NPCS0 (PB2) | SSN|
| NPCL (PD22) | CLK|

where NPCS0 (PB2) was used as a GPIO pin.

## Basic RM3100 Class functions
- `getREVID()`: Get the magnetometer's revision ID.
- `RM3100()`: Sets mode, cycleCount and chip select pin.
- `readMeasurments()`: Reads the magnetic field and updates the internal variables.
